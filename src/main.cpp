#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <MQTT.h>
#include <ArduinoJson.h>

const char ssid[] = "Mikacovi_new";
//const char ssid[] = "Y300";
const char pass[] = "";
char adresaMqttServeru[] = "mqtt.martinmikac.cz";


// nastavení jak dlouho má trvat Deep Sleep mode! - nastavení v sekundách
//const int sleepTimeS = 15;

// nastavení jak dlouho má trvat Deep Sleep mode! - nastavení v !! minutách !!
const int sleepTimeS = 10 * 60;


// Senzor vlhkosti pudy nastaveni pinu na NodeMCU
int SensorInputPin = D1; // číslo digitálního DO pinu připojeného senzoru
// Senzor vlhkosti pudy nastaveni pinu na NodeMCU
int AOpin = A0; // číslo analogového AO pinu připojeného senzoru
int senzorNapetiPin = D2; // napětí pro senzor



//nastaveni popisu senzoru a kam se budou publikovat data
char idSenzoru[] = "vlhkomer_1p_balkon";
char publishRoom[] = "Home/balcony/Humidity";



WiFiClient net;
MQTTClient client;

unsigned long lastMillis = 0;

void connect() {
  Serial.print("checking wifi...");
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(1000);
  }

  Serial.print("\nconnecting...");
  while (!client.connect("arduino", "", "")) {

    Serial.print(".");
    delay(1000);
  }

  Serial.println("\nconnected!");

  //client.subscribe("Home/BedRoom/DHT22/Temperature/");
  // client.unsubscribe("/hello");
}

void messageReceived(String &topic, String &payload) {
  Serial.println("incoming: " + topic + " - " + payload);
}

void setup() {
  Serial.begin(115200);
  WiFi.begin(ssid, pass);

  // Note: Local domain names (e.g. "Computer.local" on OSX) are not supported by Arduino.
  // You need to set the IP address directly.
  client.begin(adresaMqttServeru, net);
  client.onMessage(messageReceived);
  connect();

  // nastavení pinu A0 jako vstup
  pinMode(SensorInputPin, INPUT);

  pinMode(senzorNapetiPin, OUTPUT);

}

void loop() {
  client.loop();
  delay(10);  // <- fixes some issues with WiFi stability

  if (!client.connected()) {
    connect();
  }

  //zapneme senzor
  digitalWrite(senzorNapetiPin, HIGH);

  delay(10000);

  String vysledek_mereni = "";

  // zobrazeni stavoveho vystupu
  if (0 == digitalRead(SensorInputPin)) {
   Serial.println("Vlhko");
   vysledek_mereni = "Vlhko";
  }
  else {
   Serial.println("Sucho");
   vysledek_mereni = "Sucho";

  }


    String jsonStr;
    StaticJsonBuffer<200> jsonBuffer;
    JsonObject& root = jsonBuffer.createObject();

    // Add values in the object
    //
    // Most of the time, you can rely on the implicit casts.
    // In other case, you can do root.set<long>("time", 1351824120);
    root["Sensor_ID"] = idSenzoru;
    root["Humidity"] = vysledek_mereni;
    root.printTo(jsonStr);

    client.publish(publishRoom, jsonStr );

    delay(1000);

    digitalWrite(senzorNapetiPin, LOW);

    ESP.deepSleep(sleepTimeS * 1000000); // 15 Minuten

    delay(1000);


}
